﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }


        [HttpPost]
        public async Task<EmployeeResponse> CreateEmployeeAsync(EmployeeRequest newEmplReq)
        {
            List<Role> roles = _roleRepository.GetAllAsync().Result.ToList();
            //Employee employee = DtoMappers.EmployeeRequestToEmployee(employeeRequest, roles);

            Employee employee = new Employee()
            {
                Id = Guid.NewGuid(),
                FirstName = newEmplReq.FirstName,
                LastName = newEmplReq.LastName,
                Email = newEmplReq.Email,
                AppliedPromocodesCount = newEmplReq.AppliedPromocodesCount,
                Roles = roles.Where(r => newEmplReq.RoleIds.Contains(r.Id.ToString())).ToList()
            };

            var resultEmployee = await _employeeRepository.CreateAsync(employee);

            var employeeNew = new EmployeeResponse()
            {
                Id = resultEmployee.Id,
                Email = resultEmployee.Email,
                Roles = resultEmployee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = resultEmployee.FullName,
                AppliedPromocodesCount = resultEmployee.AppliedPromocodesCount
            };

            return employeeNew;
        }

        [HttpDelete]
        public async Task<string> DeleteEmployeeAsync(Guid id)
        {
            string res = "";
            var result = await _employeeRepository.DeleteAsync(id);
            if (result) res = "Deleted successfully";
            else res = "Deleting error";
            return res;
        }


        [HttpPut]
        public async Task<string > UpdateEmployeeAsync(EmployeeRequest emplReq)
        {
            string res = "";
            List <Role> roles = _roleRepository.GetAllAsync().Result.ToList();

            Employee employee = new Employee()
            {
                Id = emplReq.Id,
                FirstName = emplReq.FirstName,
                LastName = emplReq.LastName,
                Email = emplReq.Email,
                AppliedPromocodesCount = emplReq.AppliedPromocodesCount,
                Roles = roles.Where(r => emplReq.RoleIds.Contains(r.Id.ToString())).ToList()
            };

            var result = await _employeeRepository.UpdateAsync(employee);

            if (result) res = "Updated successfully";
            else res = "Updating error";
            return res;
        }
    }
}