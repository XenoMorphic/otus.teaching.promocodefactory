﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }
        
        public Task<T> CreateAsync(T item)
        {
            var ds = Data as ICollection<T>;
            ds.Add(item);
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == item.Id));
        }

        public Task<bool> DeleteAsync(Guid id)
        {
            var ds = Data as ICollection<T>;
            try
            {
                ds.Remove(Data.FirstOrDefault(x => x.Id == id));
                return Task.FromResult(true);
            }
            catch (Exception e)
            {
                return Task.FromResult(false);
            }            
        }

        public Task<bool> UpdateAsync(T item)
        {
            bool result = false;

            try
            {
                var element = Data.FirstOrDefault(x => x.Id == item.Id);
                var ds = Data as ICollection<T>;

                if (ds.Remove(element))
                {
                    ds.Add(item);
                    if (Data.FirstOrDefault(x => x.Id == item.Id) != null)
                        result = true;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }

            return Task.FromResult(result);
        }
    }
}